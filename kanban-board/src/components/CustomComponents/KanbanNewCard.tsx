import React, { useRef } from 'react';
import { store } from 'react-notifications-component';

const KanbanNewCard = (props: any): any => {
  const setTitleRef: any = useRef<HTMLInputElement>(null);
  const handleAdd: any = () => {
    if (
      setTitleRef.current?.value.length > 0 &&
      setTitleRef.current?.value.length < 25
    ) {
      props.onAdd({
        title: setTitleRef.current?.value,
        startOn: '',
        dueOn: '',
        assignee: '',
        cardColor: '#FFFFFF',
        label: '',
      });
    } else {
      store.addNotification({
        title: 'Error',
        message: 'Card title must be between 1 to 25 symbols!',
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
    }
  };

  const { onCancel } = props;
  return (
    <div
      style={{
        background: 'white',
        borderRadius: 3,
        border: '1px solid #eee',
        borderBottom: '1px solid #ccc',
      }}
    >
      <div style={{ padding: 5, margin: 5 }}>
        <div>
          <div style={{ marginBottom: 5 }}>
            <input type="text" ref={setTitleRef} placeholder="Title" />
          </div>
        </div>
        <button onClick={handleAdd}>Add</button>
        <button onClick={onCancel}>Cancel</button>
      </div>
    </div>
  );
};

export default KanbanNewCard;
