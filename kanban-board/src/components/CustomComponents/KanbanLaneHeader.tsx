import React from 'react';
import InlineInput from 'react-trello/dist/widgets/InlineInput';
import { Title, LaneHeader } from 'react-trello/dist/styles/Base';

const CustomLaneHeader = (props: any): any => {
  const topColor: string = props.laneColor ? props.laneColor : 'transparent';

  return (
    <div
      style={{
        borderTop: `8px solid ${topColor}`,
        borderBottom: '2px solid #c5c5c5',
        marginTop: -10,
      }}
    >
      <LaneHeader
        onDoubleClick={props.onDoubleClick}
        editLaneTitle={props.editLaneTitle}
      >
        <Title draggable={props.laneDraggable} style={props.titleStyle}>
          {props.editLaneTitle ? (
            <InlineInput
              value={props.title}
              border
              placeholder={props.t('placeholder.title')}
              resize="vertical"
              onSave={props.updateTitle}
            />
          ) : (
            props.title
          )}
        </Title>
        <div style={{ width: '30%', textAlign: 'right', fontSize: 13 }}>
          <button className="lane-del" onClick={props.onDelete}>
            X
          </button>
        </div>
      </LaneHeader>
    </div>
  );
};

export default CustomLaneHeader;
