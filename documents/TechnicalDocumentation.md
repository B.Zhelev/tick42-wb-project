## Technical Documentation    

for eslint we need in every project eslint v6.6.0 to work with react-create-app:

npm i -S @typescript-eslint/eslint-plugin @typescript-eslint/parser cross-env eslint@6.6.0 eslint-config-prettier eslint-config-react eslint-plugin-prettier eslint-plugin-sonarjs prettier   

The application can work in two modes microservices and static files. 

- Microservice mode - Each application will work on it's own port on one or multiple machines and glue42 will route requests to proper application.  
**Scheme**:  
<img src="./images/ShemeMicroservice.png" width="700px"></img>  
- Static mode - Application are builded in separate directories and glue42 serve them as a web server.   
**Scheme**:  
<img src="./images/ShemeStatic.png" width="700px"></img>
  

## Apps  

## Common components

  - components - Project components  

    - Footer  
      - Footer - application footer used in all projects

        ***Functions***  
        `toGitLab`:    
          - parameters: N/A  
          - return: void  
          - description: Open GitLab project page in new tab.  
   
    - MousePointers     
      - MousePointers - Shearable mouse pointers. Sent lockal mouse cursor position to firebase. Read other users mouse point position from firebase and show their coursors on the screen.    

        ***Input parameters:***    
        `props.boardId`: string - Opened board id.     
        `props.mousePointer`: JSON - 'x' and 'y' coordinates of the lockal mouse coursor.      
        `props.zoomBoardLevel`: number - Current zoom level.    
        `props.enableMousePointer`: boolean - State of the mouse pointer toggle.     

        ***useState:***  
        `mousePointers`: JSON - List of other users schared mouse cursor coordinates          
        `lastPointerState`: JSON - Cached local mouse pointers position used for buffering information that need to be send to firebase. It's sended every 50miliseconds.          
        `myPointer`: JSON - Local mouse pointer position.    

        ***useEffect:***   
        //Get mouse cursors from firebase - depends on `[db, boardMouseStr, setMousePointers]` - get mouse pointer collection from firebase and filter out our mouse pointer and disabled other users mouse pointers.   
        //Send mouse cursors to firebase 
        // TODO   

    - Navigation - TODO 
      - Navigation  
       
        ***Input parameters:***    
        `props.zoomBoardLevel`:     
        `props.setZoomBoardLevel`:    
        `props.enableMousePointer`:   
        `props.setEnableMousePointer`:   
        `props.boardName`:        

        ***Functions***  
        `changeLanguage`:    
          - parameters:  
            - lng: string -    
          - return: void  
          - description:      
  
        `handleBoardZoomOut`:    
          - parameters: N/A   
          - return: void  
          - description:      

        `handleBoardZoomIn`:    
          - parameters: N/A  
          - return: void  
          - description:      

        `handleBoardZoomNormal`:    
          - parameters: N/A  
          - return: void  
          - description:      

        `changeImg`:    
          - parameters: N/A  
          - return: void  
          - description:      

    - NotFound - TODO   
      - NotFound    

## Main app (react-main-app)  

  - components - Project components  

    - About  - TODO  
     - About 

      ***Functions***  
      `openLink`:    
        - parameters:  
          - link:  -    
        - return: void  
        - description:      

    - openLink
    - BoardCard  
    - CreateBoard  
    - Home  
    - InviteLink  
    - Layout  
    - Login  
    - Register  
    - User_Profile  - TODO - Rename   
    - 
  - constant  
    - BoardTemplates  
   
## Kanban app (kanban-board)   

  - App - Application main component  
//TODO: shema for the components - 
    ***useState:***  
    `zoomBoardLevel`: number - Zoom level in percentage. Displayed and changable from navigation bar used in Kanban board to zoom the component.     
    `enableMousePointer`: boolean - Toggle between enabled and disabled shared mouse pointer.     
    `boardName`: string - The name of the opened board displayed in the navigation bar.   

  - components - Project components  

    - CustomComponents - react-trello compponent replacement components  

      - KanbanCard - react-trello compponent replacement of card content, introduce custom fields  `assignee`, `startOn`, `cardColor`, `label` and delete card button     

        ***Input parameters:***    
        `props`: JSON - react-trello card.   

        ***Functions***  
        `clickDelete`:    
          - parameters: N/A  
          - return: void  
          - description: delete current card  

      - KanbanLaneHeader - react-trello compponent replacement of lane header introduce `laneColor` and delete lane button  

        ***Input parameters:***    
        `props`: JSON - react-trello lane header. 

      - KanbanNewCard - react-trello compponent replacement of new card form. Introduce validation of card title. Must be between 1 to 25 characters.

        ***Input parameters:***    
        `props`: JSON - react-trello add card handler. 

        ***Functions***  
        `handleAdd`:    
          - parameters: N/A  
          - return: void  
          - description: On create button click validate card title, create a new card and call `props.onAdd` callback with the card.  

      - KanbanNewLane - react-trello compponent replacement of new lane form. Introduce validation of lane title and lane collor selection. The title must be between 1 to 25 characters.

        ***Input parameters:***    
        `props`: JSON - react-trello add lane handler. 

         ***useState:***  
         `laneColor`: color - Changable lane color.  

        ***Functions***  
        `handleAdd`:    
          - parameters: N/A  
          - return: void  
          - description: On create button click validate lane title, create a new lane and call `props.onAdd` callback with the lane. 
      
    - Editors - Editors for board elements updating

      - CardEditor - Edit card `title`, `assignee`, `startOn`, `dueOn`, `cardColor`, `label`  

        ***Input parameters:***    
        `props.card`: JSON - JSON description of the selected cards.  
        `props.onEdit`: function onEdit(card: JSON) - Callback that it's called when submmiting changes. Accepts card JSON description.  

        ***useState:***  
        `title`: string (from 1 to 25 characters) - editable card title  
        `assignee`: string (valid e-mail) - editable assignee email  
        `startOn`: Date - editable start date
        `dueOn`:  Date (must be equal or after `startOn`)- editable due date
        `cardColor`: color - changable card color 
        `label`: string (from 0 to 35 characters) - editable card label   

        ***useEffect:***   
        Update Card Fields - depends on `[props]` - update card properties states when selected card is changed  

        ***Functions***  
        `handleEdit`:    
          - parameters: N/A  
          - return: void  
          - description: on update button click validate card properties states and call `props.onEdit` callback with updated card.    

      - LaneEditor - Edit Lane title and color  
       
        ***Input parameters:***    
        `props.lane`: JSON - JSON description of the selected lane.  
        `props.onEdit`: function onEdit(lane: JSON) - Callback that it's called when submmiting changes. Accepts lane JSON description.  

        ***useState:***  
        `title`: string (from 1 to 25 characters) - editable lane title  
        `laneColor`: color - changable lane color  

        ***useEffect:***   
        Update Lane Fields - depends on `[props]` - update `title` and `laneColor` states when selected lane is changed  

        ***Functions***  
        `handleEdit`:    
          - parameters: N/A  
          - return: void  
          - description: on update button click validate `title` and `laneColor` states and call `props.onEdit` callback with updated lane.   
      
    - KanbanBoard 
      - KanbanBoard - Kanban board component build over react-trello component with firebase cloud storage and realtime multiuser synchronization

        ***Input parameters:***    
        `props.zoomBoardLevel`: number - Zoom level in percentage.  
        `props.enableMousePointer`: boolean - toggle between enabled and disabled shared mouse pointer.    
        `props.setBoardName`: function setBoardName(name: string) - Callback to set board name in application context.   

        ***useState:***  
        `board`: JSON - JSON representation of react-trello component Kanban board. It's synhronized with the board history in firebase.      
        `lastBoard`: JSON - The last recived board from firebase. Workaround over react-trello double triggers when recieves full new board.   
        `selectedCard`: JSON - Selected card for edit.  
        `selectedLane`: JSON - Selected lane for edit.  
        `mousePointer`: JSON - Mouse pointer coordinates. 

        ***useEffect:***   
        Subscribe for board history collection - depends on `[db, boardHistoryStr]` - Get the last document from history collection when it's added or changed and set it in `board` and `lastBoard` states.  

        ***Functions***  
        `updateDB`:    
          - parameters:  
            - updatedBoard: JSON - Board state to be added to firebase board history collection.   
          - return: void  
          - description: Add board state to firebase board history collection.  

         `handleDataChange`:    
          - parameters:  
            - updatedBoard: JSON - Changed board recieved from react-trello.     
          - return: void  
          - description: Callback that recieves changes in board state.  

         `onEditCard`:    
          - parameters:  
            - card: JSON - react-trello card    
          - return: void  
          - description: Callback that recieves changed card from the card editor and use `updateDB` function to save changes in firebase.  
  
         `onCardClick`:    
          - parameters:  
            - cardId: string - Selected cardId. 
            - laneId: string - Selected card's laneId.  
          - return: void  
          - description: Open selected card and lane in the editor.   
  
        `closeEditor`:    
          - parameters: N/A  
          - return: void  
          - description: Set the selected card and lane to null to hide the editor.  

         `onCancelCardEdit`:    
          - parameters: N/A  
          - return: void  
          - description: Callback that use `closeEditor` function to close the editor.  

         `onEditLane`:    
          - parameters:  
            - lane: JSON - react-trello lane   
          - return: void  
          - description: Callback that recieves changed lane from the lane editor and use `updateDB` function to save changes in firebase.  
  
         `onMouseMove`:    
          - parameters:  
            - e: JSON - event structure for mouse move.     
          - return: void  
          - description: Call `props.setMousePointer` to set `x` and `y` mouse pointer coordinates in application context. 
   
    - SideMenu
      - Menu - Container for lane and card editors. 

  
## Database app (database-board-app)   

  - App - Application main component  
  //TODO: shema for the components -   
      ***useState:***  
      `zoomBoardLevel`: number - Zoom level in percentage. Displayed and changable from navigation bar used in Database board to zoom the component.     
      `enableMousePointer`: boolean - Toggle between enabled and disabled shared mouse pointer.     
      `boardName`: string - The name of the opened board displayed in the navigation bar.   


      <DatabaseBoard
                    {...props}
                    zoomBoardLevel={zoomBoardLevel}
                    enableMousePointer={enableMousePointer}
                    setBoardName={setBoardName}
                  />

  - components - Project components  
    
    - DatabaseBoard  
      - DatabaseBoard  
        
        ***Input parameters:***    
        `props.zoomBoardLevel`: number - Zoom level in percentage.  
        `props.enableMousePointer`: boolean - toggle between enabled and disabled shared mouse pointer.    
        `props.setBoardName`: function setBoardName(name: string) - Callback to set board name in application context.

        ***useState:***  
        `refresh`: boolean - State is used to force react-database-diagram component to redraw.          
        `grid`: JSON - Content of the table editor. It contains currently selected table schema.          
        `config`: JSON - Configuration of the react-database-diagram component.    
        `selectedTable`: JSON - Contains shema of the table selected for editing.     
        `selectedTableName`: string - The name of the selected table. Can be different form the name of `selectedTable` because it could be edited but not submited yet and the name into `selectedTable` is not updated at this point.    
        `schema`: IDatabaseTable - The database schema displayed by react-database-diagram component. It's synhronized with the board history in firebase.        

        ***useEffect:***   
        Subscribe for board history collection - depends on `[db, boardHistoryStr]` - Get the last document from history collection when it's added or changed and set it in `board` and `lastBoard` states. Sort `schema` list by table FK number to force react-database-diagram component to autoalign properly.     

        ***Functions***  
        `updateDB`:    
          - parameters:  
            - updatedBoard: JSON - Board state to be added to firebase board history collection.   
          - return: void  
          - description: Add board state to firebase board history collection.  

        `handleTableClick`:    
          - parameters:  
            - table: JSON - Table schema to opened for edit.        
          - return: void  
          - description: Call `openTable` function on table list click to open the selected table in the table editor.      

        `openTable`:    
          - parameters:  
            - table: JSON - Table schema to opened for edit.         
          - return: void  
          - description: Set `selectedTable` and `selectedTableName` states to open the selected table in the table editor.        
  
        `handleEditSubmit`:    
          - parameters: N/A    
          - return: void  
          - description: Validate all fields from the table editor. Convert them to react-database-diagram component schema and add them to firebase board history.     
  
        `handleAddTable`:    
          - parameters: N/A  
          - return: void  
          - description: Generate name of the table in format `"new_tableX"` where `X` is the next free number. Create empty table and add it to react-database-diagram component schema.    
  
        `handleAddRow`:    
          - parameters: N/A      
          - return: void  
          - description: Create emty row and add it to the table editor.    

        `handleDeleteTable`:    
          - parameters: N/A    
          - return: void  
          - description: Check for FK pointing to the selected table. If there are no FK delete the table.      

        `valueRender`:    
          - parameters:  
            - cell: JSON - `react-datasheet` component cell.       
          - return: void  
          - description: Callback for `react-datasheet` component. Show the real value in edit mode.        
  
        `dataRender`:    
          - parameters:  
            - cell: JSON - `react-datasheet` component cell.           
          - return: void  
          - description: Callback for `react-datasheet` component. Show data or component when is not in edit mode.     
  
        `onCellsChanged`:    
          - parameters:  
            - changes: JSON - The changes in `react-datasheet` component cells.         
          - return: void  
          - description: Callback for `react-datasheet` component. Update the grid state with the changes from the editor.     
  
        `onContextMenu`:    
          - parameters:  
            - e: JSON - `react-datasheet` component event.    
            - cell: JSON - `react-datasheet` component cell.       
          - return: void  
          - description: Callback for `react-datasheet` component. Disable context menu.      



