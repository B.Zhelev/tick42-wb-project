import React, { useContext, useEffect, useState } from 'react';

import * as firebase from 'firebase/app';
import 'firebase/firestore';
import '../../constants/base';
import DatabaseDiagram, {
  IDatabaseTable,
} from '@cybertec/react-database-diagram';
import '../../static/css/DatabaseBoard.css';
import { Button, Col, ListGroup, Row } from 'react-bootstrap';
import Datasheet from 'react-datasheet';
import 'react-datasheet/lib/react-datasheet.css';
import '../../static/css/Menu.css';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import { Input } from 'reactstrap';
import { store } from 'react-notifications-component';
import { useHistory } from 'react-router';
import MousePointers from '../MousePointers/MousePointers';

const DatabaseBoard = (props: any) => {
  const history = useHistory();
  const [mousePointer, setMousePointer] = useState({ x: 0, y: 0 });

  const boardId: string | undefined = window.location.pathname.split('/').pop();
  const user: any = firebase.auth().currentUser;
  const db: any = firebase.firestore();

  const boardDoc: any = db.collection(`boards`).doc(boardId);
  boardDoc.get().then((doc: any) => props.setBoardName(`Name: ${doc.data().name}`))
    .catch(() => history.push('/notfound'));

  const boardHistoryStr = `boards/${boardId}/history`;

  const ALL_CONSTRAINTS = ['', 'NOT NULL', 'UNIQUE', 'CHECK', 'DEFAULT'];
  const DB_TYPES = ['BOOLEAN', 'INTEGER', 'DOUBLE', 'DATETIME'];

  const { theme } = useContext(ThemeContext);

  const [refresh, setRefresh] = useState(true);

  const [grid, setGrid] = useState([
    [
      { value: 'Column name', readOnly: true },
      { value: 'Type', readOnly: true },
      { value: 'Constrains', readOnly: true },
      { value: 'Relations to', readOnly: true },
    ],
  ]);

  const [config] = useState({
    allowLooseLinks: false,
    allowCanvasTranslation: false,
    allowCanvasZoom: false,
    maxNumberPointsPerLink: 10,
    smartRouting: false,
  });
  const [selectedTable, setselectedTable] = useState({} as any);
  const [selectedTableName, setSelectedTableName] = useState('');

  //JSON structure for db - it's my cheet sheet, please don't delete it :)
  /*const [schema,setSchema] = useState([
  {
    columns: [
      { name: "a", type: "integer NOT NULL" },
      { name: "b", type: "integer NOT NULL" },
      { name: "c", type: "integer" }
    ],
    table_name: "t1",
    foreign_keys: [
      {
        toTable: "t2",
        toSchema: "public",
        toColumns: ["c"],
        fromColumns: ["c"]
      },
    ],
    primary_keys: ["a"],
    table_schema: "cypex_generated"
  },
  {
    columns: [{ name: "c", type: "integer" }],
    table_name: "t2",
    foreign_keys: [],
    table_schema: "cypex_generated"
  },
    {
      columns: [{ name: "c", type: "integer" }],
      table_name: "t3",
      foreign_keys: [],
      table_schema: "cypex_generated"
    },
  ] as any | IDatabaseTable);*/

  const [schema, setSchema] = useState([] as any | IDatabaseTable);

  // Subscribe for board history collection  
  useEffect(() => {
    db.collection(boardHistoryStr)
      .orderBy('time', 'desc')
      .limit(1)
      .onSnapshot(function (snapshot: any) {
        snapshot.forEach(function (newBoard: any) {
          const sortedByFK = newBoard.data().schema.sort((a: any, b: any) => b.foreign_keys.length - a.foreign_keys.length);
          setSchema(sortedByFK); //TODO: function for that
          setRefresh(false);
          setTimeout(function () {
            setRefresh(true);
          }, 0);
        });
      });
  }, [db, boardHistoryStr]);

  const updateDB = (newSchema: any): void => {
    const boardHistory = db.collection(boardHistoryStr);
    const updatedBoard: any = {
      schema: newSchema,
      time: firebase.firestore.FieldValue.serverTimestamp(),
      user: user.uid,
    };
    console.log("test" + updatedBoard);
    boardHistory.doc().set(updatedBoard);
  };

  const handleTableClick = (table: any) => {
    openTable(table);
  };

  const openTable = (table: any) => {
    setselectedTable(table);
    setSelectedTableName(table.table_name);
    const editor = table.columns.map((o: any) => {
      const pk: number = table.primary_keys
        ? table.primary_keys.findIndex((e: any) => e === o.name)
        : -1;
      const type: string = o.type.split(' ')[0];
      const constrain: string =
        (pk > -1 ? 'PK ' : '') + o.type.split(' ').slice(1).join(' ');
      const relations = table.foreign_keys
        ? table.foreign_keys.filter((e: any) => e.fromColumns.includes(o.name))
        : [];
      const relationsStr: string = relations
        .map((e: any) => e.toTable + '.' + e.toColumns[0])
        .join(' ');
      return [
        { value: o.name, readOnly: false },
        { value: type },
        { value: constrain },
        { value: relationsStr },
      ];
    });
    const newGrid = [grid[0], ...editor];
    setGrid(newGrid);
  };

  const handleEditSubmit = () => {
    try {
      if (!/^[^\\/?%*:|\"<>.]{1,64}$/.test(selectedTableName)) {
        throw new Error(
          `Invalid table name "${selectedTableName}". Table name must meet the standard table name requirements for SQL table.`,
        );
      }
      const columns = grid
        .slice(1)
        .map((o) => {
          if (o[0].value === '') {
            return undefined;
          }
          if (!/^[^\\/?%*:|\"<>.]{1,64}$/.test(o[0].value)) {
            throw new Error(
              `Invalid column name "${o[0].value}". Column name must meet the standard column name requirements for SQL table.`,
            );
          }
          if (
            !/(^VARCHAR\(([1-9][0-9]{0,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])\)$)/.test(
              o[1].value.toUpperCase(),
            ) &&
            !/(^BLOB\(([1-9][0-9]{0,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])\)$)/.test(
              o[1].value.toUpperCase(),
            ) &&
            !DB_TYPES.includes(o[1].value.toUpperCase())
          ) {
            throw new Error(`Invalid type: ${o[1].value}`);
          }
          const constraints = o[2].value
            .replace('NOT NULL', 'NOT_NULL')
            .split(' ')
            .filter((e) => e !== 'PK')
            .map((e) => e.replace('NOT_NULL', 'NOT NULL'))
            .map((e) => {
              if (ALL_CONSTRAINTS.includes(e.toUpperCase())) {
                return e;
              }
              throw new Error(`Invalid constraint: ${o[2].value}`);
            })
            .join(' ');
          return {
            name: o[0].value,
            type: o[1].value + (constraints !== '' ? ' ' + constraints : ''),
          };
        })
        .filter((o) => o !== undefined); //remove empty rows
      const primaryKeys = grid
        .slice(1)
        .filter((o) => o[2].value.includes('PK'))
        .map((e) => e[0].value);
      const foreignKeysRows: any = grid
        .slice(1)
        .filter((o) => o[3].value !== '');
      const foreignKeys: any = [];
      foreignKeysRows.forEach((o: any) => {
        o[3].value.split(' ').forEach((e: any) => {
          const [toTable, toColumn] = e.split('.');
          if (!toTable || !toColumn) {
            throw new Error(
              `Bad FK's format "${o[3].value}". Format must be "tablename.columnname."`,
            );
          }

          if (!/^[^\\/?%*:|\"<>.]{1,64}$/.test(toTable)) {
            throw new Error(
              `Invalid FK table name "${toTable}". Table name must meet the standard table name requirements for SQL table.`,
            );
          }
          if (!/^[^\\/?%*:|\"<>.]{1,64}$/.test(toColumn)) {
            throw new Error(
              `Invalid FK column name "${toColumn}". Column name must meet the standard column name requirements for SQL table.`,
            );
          }
          if (schema.findIndex((o: any) => o.table_name === toTable) === -1) {
            throw new Error(
              `Invalid FK table name "${toTable}". Table "${toTable}" doesn't exists.`,
            );
          }
          foreignKeys.push({
            toTable: toTable,
            toSchema: 'cypex_generated',
            toColumns: [toColumn],
            fromColumns: [o[0].value],
          });
        });
      });

      const updatedTable = {
        table_name: selectedTableName,
        table_schema: 'cypex_generated',
        columns: columns,
        primary_keys: primaryKeys,
        foreign_keys: foreignKeys,
      };
      const newSchema = schema.map((o: any) =>
        o.table_name === selectedTable.table_name ? updatedTable : o, // find the table from tables we want to update
      );
      updateDB(newSchema);
      setselectedTable(updatedTable);

      const newGrid = grid.filter((o) => o[0].value !== ''); // remove emty rows from the table editors
      setGrid(newGrid);
    } catch (e) {
      store.addNotification({
        title: 'Error',
        message: e.message,
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
    }
  };

  const handleAddTable = () => {
    const newTableNums = schema
      .filter((o: any) => o.table_name.startsWith('new_table'))
      .map((o: any) => Number(o.table_name.replace('new_table', '')))
      .sort((a: number, b: number) => b - a);
    const tableNum = newTableNums.length ? newTableNums[0] + 1 : 1;
    const newTable = {
      table_name: `new_table${tableNum}`,
      table_schema: 'cypex_generated',
      columns: [],
      primary_keys: [],
      foreign_keys: [],
    };
    const newSchema = [...schema, newTable];
    updateDB(newSchema);
    openTable(newTable);
  };

  const handleAddRow = () => {
    const updatedGrid = [...grid];
    updatedGrid.push([
      { value: '', readOnly: false },
      { value: '', readOnly: false },
      { value: '', readOnly: false },
      { value: '', readOnly: false },
    ]);
    setGrid(updatedGrid);
  };

  const handleDeleteTable = () => {
    const result = window.confirm("Are you sure you want to delete this table?");
    if (result === false) {
      return;
    }
    if (schema.filter((o: any) => o.foreign_keys.findIndex((e: any) => e.toTable === selectedTableName) > -1).length > 0) {
      store.addNotification({
        title: 'Error',
        message: "The table has relations and can't be deleted. Remove FK that point to this table first.",
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
      return;
    }
    const newSchema = schema.filter(
      (o: any) => o.table_name !== selectedTableName,
    );
    updateDB(newSchema);
  };

  const valueRender = (cell: any) => cell.value; // show the real value in edit mode

  const dataRender = (cell: any) => // show data or component when is not in edit mode.
    cell.component ? cell.component : cell.value;

  const onCellsChanged = (changes: any) => { // update the grid state with the changes from the editor.  
    const newGrid: any = grid;
    changes.forEach(({ row, col, value }: any) => {
      newGrid[row][col] = { ...newGrid[row][col], value };
    });
    setGrid(newGrid);
  };
  
  const onContextMenu = (e: any, cell: any) =>
    cell.readOnly ? e.preventDefault() : null;

  const onMouseMove = (e: any) => {
    setMousePointer({ x: e.clientX, y: e.clientY });
  };

  return (
    <>
      <div
        className={`kanban-background-${theme.background}`}
        style={{ paddingTop: 64 }}
      >
        <Row style={{ width: '97.8vw', margin: '0' }}>
          <Col className="col-lg-2 table-names">
            <Button
              variant="outline-primary"
              className="btn-add-table"
              onClick={handleAddTable}
            >
              Add Table
            </Button>
            <ListGroup>
              {schema.map((o: any) => (
                <ListGroup.Item
                  action
                  active={o.table_name === selectedTableName}
                  key={o.table_name}
                  onClick={() => handleTableClick(o)}
                  style={{lineHeight: 0.1, verticalAlign: "middle", cursor: "pointer"}}
                >
                  {o.table_name}
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Col>

          <Col className="col-lg-6 edit-table">
            <div className="wrapper-edit-table">
              <Row className="justify-content-center">
                <Row>
                  <label className="label-name">Table Name: </label>
                  <span>
                    <Input
                      className="input-table-name"
                      type="text"
                      value={selectedTableName}
                      onChange={(e) => setSelectedTableName(e.target.value)}
                    />
                  </span>
                </Row>
                <div>
                  <Button
                    variant="outline-success"
                    className="btn btn-submit"
                    onClick={handleEditSubmit}
                  >
                    {' '}
                    Submit
                  </Button>
                </div>
                <div>
                  <Button
                    variant="outline-primary"
                    className="btn-add-row"
                    onClick={handleAddRow}
                  >
                    Add Row
                  </Button>
                </div>
                <div>
                  <Button
                    variant="outline-danger"
                    className="btn btn-delete-table"
                    onClick={handleDeleteTable}
                  >
                    {' '}
                    Delete Table
                  </Button>
                </div>
                <div>
                  <Button variant="outline-primary" className="btn btn-cancel">
                    Cancel
                  </Button>
                </div>
              </Row>
            </div>
            <Row>
              <Datasheet
                data={grid}
                valueRenderer={valueRender}
                dataRenderer={dataRender}
                onContextMenu={onContextMenu}
                onCellsChanged={onCellsChanged}
                className="editable-listview"
              />
            </Row>
          </Col>
          <Col className="legend">
            <label className="legend-title">Legend</label>
            <Row>
              <Col className="col-md-12 legend-text">
                <label className="legend-types">Types:</label>
                <table
                  style={{
                    border: '1px solid black',
                    borderCollapse: 'collapse',
                  }}
                >
                  <tr>
                    <td style={{ width: 1000 }}>VARCHAR(1..65535)</td>
                    <td style={{ width: 1000 }}>string</td>
                  </tr>
                  <tr>
                    <td>BLOB(1..65535)</td>
                    <td>binary data</td>
                  </tr>
                  <tr>
                    <td>BOOLEAN</td>
                    <td>true or false</td>
                  </tr>
                  <tr>
                    <td>INTEGER</td>
                    <td>integer number</td>
                  </tr>
                  <tr>
                    <td>DOUBLE</td>
                    <td>real number</td>
                  </tr>
                  <tr>
                    <td>DATETIME</td>
                    <td>&apos;YYYY-MM-DD hh:mm:ss&apos;</td>
                  </tr>
                </table>
              </Col>
              <Col className="col-md-1"></Col>
            </Row>
            <Row>
              <Col>
                <label className="legend-constrains">Constrains:</label>
                <div>NOT NULL</div>
                <div>UNIQUE</div>
                <div>CHECK</div>
                <div>DEFAULT</div>
              </Col>
              <Col>
                <label className="legend-constrains">Relations:</label>
                <div>The form is - table_name.column_name</div>
                <div>For more than one table -</div>
                <div>t1.c1 t2.c2 t3.c3  etc.</div>
              </Col>
            </Row>
          </Col>
        </Row>

        <div>
          <div
            style={{
              width: '98.7vw',
              height: '60vh',
              position: 'absolute',
              zIndex: 1,
            }}
            onClick={(e) => {
              e.preventDefault();
            }}
            onMouseMove={onMouseMove}
          />
          <div
            style={{
              width: '98.6vw',
              height: '60vh',
              pointerEvents: 'none',
              color: 'black',
              fontSize: 4,
              transform: `scale(${Math.abs(props.zoomBoardLevel) / 100})`,
              transformOrigin: '0 0',
              marginLeft: 10,
              marginTop: 10,
            }}  
          >
            {refresh && <DatabaseDiagram schema={schema} config={config} />}
          </div>
        </div>
      </div>
      <MousePointers
        boardId={boardId}
        mousePointer={mousePointer}
        zoomBoardLevel={props.zoomBoardLevel}
        enableMousePointer={props.enableMousePointer}
      />
    </>
  );
};

export default DatabaseBoard;
