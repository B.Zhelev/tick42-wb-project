import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { AuthProvider } from './Context/AuthContext/Auth';
import ThemeProvider from './Context/Theme/ThemeContext';
import UserProfile from './component/User_Profile/UserProfile';
import Home from './component/Home/Home';
import Register from './component/Register/Register';
import Login from './component/Login/Login';
import ProtectedRoute from './Routes/ProtectedRoute/ProtectedRoute';
import ReactNotifications from 'react-notifications-component';
import About from './component/About/About';
import BoardPage from './component/BoardPage/BoardPage';
import NotFound from './component/NotFound/NotFound';
import ForgottenPassword from './component/Login/ForgottenPassword';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Context/Theme/ThemeContext.css';

import 'react-notifications-component/dist/theme.css';
import './static/css/App.css';
import CreateBoard from './component/CreateBoard/CreateBoard';
import InvitePage from './component/InviteLink/InvitePage';
import GlueWeb from "@glue42/web";
import GlueWorkspaces from '@glue42/workspaces-api';
import { GlueProvider, Glue42ReactFactory } from '@glue42/react-hooks';
import Layout from './component/Layout/Layout';

//To-do add proper types
const App = () => {

  return (
    <>
      <GlueProvider config={{ channels: true, appManager: true, application: 'Clients', libraries: [GlueWorkspaces] }} glueFactory={GlueWeb as Glue42ReactFactory}>
        <Router>
          <ReactNotifications />
            <ThemeProvider>
              <AuthProvider >
                <Layout>
                  <Switch>
                    <Route exact path='/home' component={Home} />
                    <ProtectedRoute exact path='/profile/createBoard' component={CreateBoard} />
                    <ProtectedRoute exact path='/boardPage' component={BoardPage} />
                    <Redirect exact path='/' to='/home' />
                    <Route exact path='/register' component={Register} />
                    <Route exact path='/profile/invitepage/:boardId' component={InvitePage} />
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/about' component={About} />
                    <ProtectedRoute exact path='/profile' component={UserProfile} />
                    <Route exact path='/forgottenpassword' component={ForgottenPassword} />
                    <Route path="*" component={NotFound} />
                  </Switch>
                </Layout>
              </AuthProvider>
          </ThemeProvider>
        </Router>
      </GlueProvider>
    </>
  );
}

export default App;