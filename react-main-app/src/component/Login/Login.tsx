import React, { useState, useContext } from 'react'
import FireBaseApp from '../../constant/base';
import * as firebase from 'firebase/app'
import { withRouter } from 'react-router-dom';
import login from '../../static/img/login.png';
import check from '../../static/img/checked.png';
import uncheck from '../../static/img/unchecked.png';
import fb from '../../static/img/fb.png';
import google from '../../static/img/google.png';
import github from '../../static/img/github.png';
import github_dark from '../../static/img/github_dark.png';
import { Row, Button } from 'react-bootstrap';
import { Input } from 'reactstrap';
import { store } from 'react-notifications-component';
import '../../static/css/LoginReg.css';
import { ThemeContext } from '../../Context/Theme/ThemeContext';

//todo fix types
const Login = (props: any) => {

  const [imgChecked, setImgChecked] = useState(uncheck);
  const [emailValid, setEmailValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const { theme } = useContext(ThemeContext)
  const providerGmail: any = new firebase.auth.GoogleAuthProvider();
  const providerFB = new firebase.auth.FacebookAuthProvider();
  const providerGitHub = new firebase.auth.GithubAuthProvider();


  const handleEmailChange = (e: any): void => {
    if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(e.target.value)) {
      setEmailValid(true);
    }
    else {
      setEmailValid(false);
    }
  }

  const handlePasswordChange = (e: any): void => {
    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/.test(e.target.value)) {
      setPasswordValid(true);
    }
    else {
      setPasswordValid(false);
    }
  }

  const toRegister = () => {
    props.history.push('/register');
  }

  const handleReset = () => {
    props.history.push('/forgottenpassword');
  }

  const handleGoogleLogin = (): void => { //TODO: delete after inf dont need to use it
    firebase.auth().signInWithPopup(providerGmail).then(function (result) {
      // The signed-in user info.
      //const user: any = result.user;
      props.history.push('/home');
    })

    .catch(function (error) {
      store.addNotification({
        title: 'Error',
        message: 'Login faild',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    });
  }

  const handleFbLogin = (): void => { //TODO: delete after inf dont need to use it
    firebase.auth().signInWithPopup(providerFB).then(function (result) {
      // The signed-in user info.
      //const user: any = result.user;
      props.history.push('/home');
    }).catch(function (error) {
      store.addNotification({
        title: 'Error',
        message: 'Login faild',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    });
  }

  const handleGitHubLogin = (): void => { //TODO: delete after inf dont need to use it but we'll need them soon
    firebase.auth().signInWithPopup(providerGitHub).then(function (result) {
      // The signed-in user info.
      //const user: any = result.user;
      props.history.push('/home');
      // ...
    }).catch(function (error) {
      store.addNotification({
        title: 'Error',
        message: 'Login faild',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    });
  }

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    /*if (!passwordValid) {
      return store.addNotification({
        title: 'Error',
        message: 'Must contain at least one number, one uppercase and lowercase letter, one spacial character and from 8 to 15 characters!',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }*/

    const { email, password } = event.target.elements;
    try {
      await FireBaseApp
        .auth()
        .signInWithEmailAndPassword(email.value, password.value)
      props.history.push('/home')
    } catch (err) {
      store.addNotification({
        title: 'Error',
        message: err.message,
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }
  }

  return (
    <>
      <div className="log-reg-wrapper">
        <div className="log-reg-container">
          <div className="login-reg-circles">
            <span className="r" />
            <span className="r-s" />
            <span className="l-s" />
            <span className="l" />
          </div>
          <div>
            <div className={`auth-inner authInner bg-${theme.background}`}>
              <form onSubmit={handleSubmit}>
                <div className="log-reg-ico"><img alt="Not Found" src={login} /></div>
                <div className="log-reg-title standartText">Login</div>

                <div className="login-have-account">
                  <Row>
                    <div className="forgot-password">Don't have an account?</div>
                    <div className="log-reg-redirect" onClick={toRegister}>Sign up</div>
                  </Row>
                </div>

                <div className="form-group">
                  <div className="labels-login-reg standartText">Email address</div>
                  <Input type="email" id='email' className={`logReg-form-control-${theme.background}`} placeholder="Enter email"
                    valid={emailValid ? true : false} onChange={handleEmailChange} />
                </div>

                <div className="form-group">
                  <div className="labels-login-reg standartText">Password</div>
                  <Input type="password" id='password' className={`logReg-form-control-${theme.background}`} placeholder="Enter password"
                    valid={passwordValid ? true : false} onChange={handlePasswordChange} />
                </div>

                <div className="wrapper-login-reg">
                  <Button type="submit" className="btn btn-login-reg">Login</Button>
                </div>

                <div className="login-have-account">
                  <Row>
                    <div className="forgot-password">Forgot password?</div>
                    <div className="reset-password" onClick={handleReset}>Reset</div>
                  </Row>
                </div>

                <span className="auth-sep standartText"> or Sign up with one click </span>

                <div className="fast-icons">
                  <Row>
                    <div className="fb-ico" onClick={handleFbLogin}><img alt="Not Found" src={fb} /></div>
                    <div className="google-ico" onClick={handleGoogleLogin}><img alt="Not Found" src={google} /></div>
                    <div className="github-ico" onClick={handleGitHubLogin}><img alt="Not Found" src={theme.text === 'dark'
                      ? github
                      : github_dark} /></div>
                  </Row>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default withRouter(Login);