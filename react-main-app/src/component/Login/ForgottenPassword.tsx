import React, { useContext, useState } from 'react'
import '../../constant/base';
import * as firebase from 'firebase/app'
import { withRouter } from 'react-router-dom';
import login from '../../static/img/login.png';
import { Button } from 'react-bootstrap';
import { Input } from 'reactstrap';
import { store } from 'react-notifications-component';
import '../../static/css/LoginReg.css';
import { ThemeContext } from '../../Context/Theme/ThemeContext';

//todo fix types
const ForgottenPassword = (props: any) => {
  const [emailValid, setEmailValid] = useState(false);
  const { theme } = useContext(ThemeContext);

  const handleEmailChange = (e: any): void => {
    if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(e.target.value)) {
      setEmailValid(true);
    }
    else {
      setEmailValid(false);
    }
  }

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const { email } = event.target.elements;
    try {
      const auth = firebase.auth();
      auth.sendPasswordResetEmail(email.value).then(function () {
        store.addNotification({
          title: 'Success',
          message: 'Email send.',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                  // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      }).catch(function (error) {
        store.addNotification({
          title: 'Error',
          message: 'Failed to send.',
          type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                  // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      });
      props.history.push('/login')
    } catch (err) {
      store.addNotification({
        title: 'Error',
        message: err.message,
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }
  }

  return (
    <>
      <div className="log-reg-wrapper">
        <div className="log-reg-container">
          <div className="login-reg-circles">
            <span className="r" />
            <span className="r-s" />
            <span className="l-s" />
            <span className="l" />
          </div>
          <div>
            <div className={`auth-inner authInner bg-${theme.background}`}>
              <form onSubmit={handleSubmit}>
                <div className="log-reg-ico"><img alt="Not Found" src={login} /></div>
                <div className="log-reg-title standartText">Login</div>

                <div className="form-group">
                  <div className="labels-login-reg standartText">Email address</div>
                  <Input type="email" id='email' className="form-control formControl" placeholder="Enter email"
                    valid={emailValid ? true : false} onChange={handleEmailChange} />
                </div>

                <div className="wrapper-login-reg">
                  <Button type="submit" className="btn btn-login-reg">Send</Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default withRouter(ForgottenPassword);
