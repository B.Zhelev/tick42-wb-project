import React, { useState, useContext, useEffect } from 'react';
import { Container, Jumbotron, Figure, Card, Button, Form, CardGroup, FormControl, InputGroup, CardColumns, Nav, Row } from 'react-bootstrap';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../Context/AuthContext/Auth';
import BoardCard from '../BoardCard/BoardCard';
import firebase from 'firebase';
import { store } from 'react-notifications-component';

const UserProfile = (props: any) => {
    const { user, setUser } = useContext(AuthContext)
    const [createdOrGuest, setCreatedOrGuest] = useState(false)
    const [CreatedBoards, setCreatedBoards] = useState(user.createdBoards)
    const [VistingBoards, setVistingBoards] = useState(user.visitedBoards)
    const [listBoardState, setBoard] = useState('');
    const [changeUsername, setChangeUsername] = useState(false);
    const [changePassword, setChangePassword] = useState(false);
    const [uploadPhoto, setUploadPhoto] = useState(false);
    const [photo, setPhoto] = useState(null)
    const { theme } = useContext(ThemeContext)

    const isEmailAccount = firebase.auth().currentUser!.providerData[0]!.providerId === 'password';

    const getPhoto = async () =>{
        try{
            setPhoto(await firebase.storage()
            .ref(`ProfilePics/${user.uid}`)
            .getDownloadURL())
        }catch(err){
            setPhoto(await firebase.storage()
            .ref(`pic.png`)
            .getDownloadURL())
        }
    } 
    useEffect(()=>{
        getPhoto()
    },[])
    console.log("11111" +photo)
    const onFormSubmit = (event: any) => {
        event.preventDefault();
        setCreatedBoards(user.createdBoards)
        setVistingBoards(user.visitedBoards)
        const filterFunc = (arr: any[]) => {

            return arr.filter((board: any) => {
                 if (BoardTypes === 'All' ? false : board.type !== BoardTypes) {
                     return false;
                 }
 
                 if (!(onlyPrivate && onlyPublic)) {
                     if (onlyPrivate ? board.isPrivate !== onlyPrivate : onlyPrivate) {
                         return false;
                     }
 
                     if (onlyPublic ? board.isPrivate === onlyPublic : onlyPublic) {
                         return false;
                     }
                 }
 
                 if (!board.name.toLowerCase().includes(searchString.toLowerCase())) {
                     return false;
                 }
 
                 return true;
             });
         };

        const allQueries: any[] = Array.from(event
            .target
            .elements)

        const BoardTypes: any = allQueries
            .filter((el: any) => el.id === 'BoardType')
            .pop()
            .value

        const onlyPrivate: any = allQueries
            .filter((el: any) => el.id === 'OnlyPrivate')
            .pop()
            .checked


        const onlyPublic: any = allQueries
            .filter((el: any) => el.id === 'OnlyPublic')
            .pop()
            .checked


        const searchString: any = allQueries
            .filter((el: any) => el.id === 'BoardSearchString')
            .pop()
            .value

        const showCreated: any = allQueries
            .filter((el: any) => el.id === 'OnlyCreated')
            .pop()
            .checked

        if (showCreated) {
            setCreatedOrGuest(true)
        } else {
            setCreatedOrGuest(false)
        }
        console.log(user)
        if (createdOrGuest) {
            setCreatedBoards(filterFunc(user.createdBoards))
        } else {
            setVistingBoards(filterFunc(user.visitedBoards))
        }
            
    }

    const onPhotoSubmit = async (e: any) => {
        e.preventDefault();
        const file: any = Array.from(e.target.elements)
        .filter((el: any) => el.id === 'photoUpload')
        .pop()

       const newPhotoRef = await firebase.storage()
        .ref(`ProfilePics/${user.uid}`)
      await newPhotoRef.put(file.files[0])
        getPhoto()
        setUploadPhoto(false)
    }

    const onUsernameSubmit = async (e: any) => {
        e.preventDefault();
        const userNameControl: any = Array.from(e.target.elements)
            .filter((el: any) => el.id === 'Username')
            .pop();
        const userName = userNameControl.value;

        if (userName < 1 || userName > 50) {
            return store.addNotification({
                title: 'Error',
                message: 'Username must be from 1 to 50 characters!',
                type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
                container: 'top-right',                  // where to position the notifications
                animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                dismiss: {
                    duration: 3000
                }
            });
        }

        const updateUserRef = firebase
            .firestore()
            .collection('Users')
            .doc(user.uid);

        await updateUserRef.get().then(async (doc) => {
            if (!doc.exists) {
                //Should be impossible to reach here
                return;
            }
            else {
                const updateUser = { ...doc.data(), username: userName };
                await updateUserRef.set(updateUser);
                setChangeUsername(false);
            }
        })
    }

    const onPasswordSubmit = async (e: any) => {
        e.preventDefault();
        const passwordControl: any = Array.from(e.target.elements)
            .filter((el: any) => el.id === 'Password')
            .pop();
        const confirmPasswordControl: any = Array.from(e.target.elements)
            .filter((el: any) => el.id === 'ConfirmPassword')
            .pop();
        const password = passwordControl.value;
        const confirmPassword = confirmPasswordControl.value;

        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/.test(password)) {
            return store.addNotification({
                title: 'Error',
                message: 'Must contain at least one number, one uppercase and lowercase letter, one spacial character and from 8 to 15 characters!',
                type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
                container: 'top-right',                  // where to position the notifications
                animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                dismiss: {
                    duration: 3000
                }
            });
        }

        if (password !== confirmPassword) {
            store.addNotification({
                title: 'Error',
                message: `Your passwords doesn't match!`,
                type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
                container: 'top-right',                  // where to position the notifications
                animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                dismiss: {
                    duration: 3000
                }
            });
        }

        firebase.auth().currentUser!.updatePassword(password).then(() => {
            store.addNotification({
                title: 'Success',
                message: 'Password changed.',
                type: 'success',                          // 'default', 'success', 'info', 'warning', 'danger'
                container: 'top-right',                  // where to position the notifications
                animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                dismiss: {
                    duration: 3000
                }
            });
        }, (error: any) => {
                store.addNotification({
                    title: 'Error',
                    message: error.message,
                    type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
                    container: 'top-right',                  // where to position the notifications
                    animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                    animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                    dismiss: {
                        duration: 3000
                    }
                });
        });
        setChangeUsername(false);
    }

    return (
        <Container className='rounded-bottom' style={{ paddingBottom: 48 }}>
            <span className="rr" />
            <span className="rr-s" />
            <span className="ll-s" />
            <span className="ll" />
            <Jumbotron style={{ marginTop: '30px'}} className={`bg-${theme.background}`}>
                <h3 className="d-flex justify-content-center">{`Hi, ${user!.username}!`}</h3>
                <Figure className="d-flex justify-content-center">
                    {photo && <Figure.Image
                        width={'15%'}
                        height={'15%'}
                        alt="ERROR"
                        src={`${photo}`}
                    />}
                </Figure>
                
                <br />
                <div className={`text-center`}>

                    <h5 className={`bg-${theme.background}`}>Email: {`${user!.email}`} </h5>
                    {uploadPhoto ?
                    <Form onSubmit={onPhotoSubmit}>
                        <Row>
                        <Button style={{ zIndex: 1000 }} variant={`outline-${theme.text}`} type='submit'>Set Photo</Button>
                        <Form.File 
                            id="photoUpload"
                            label="Custom file input"
                            custom
                            style={{ width: '90%' }}
                        />
                        </Row>
                    </Form>  
                  : <Button className={'mx-auto'} variant={`outline-${theme.text}`} onClick={() => {setUploadPhoto(true)}} >Change Photo</Button>  }
                    <br /><br />
                    {changeUsername ?
                        <Container>
                            <Form onSubmit={onUsernameSubmit}>
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <Button variant={`outline-${theme.text}`} type='submit'>Change Username</Button>
                                </InputGroup.Prepend>
                                <FormControl
                                    id="Username"
                                    placeholder={`${user!.username}`}
                                    aria-label="Username"
                                    aria-describedby="basic-addon1"
                                />
                            </InputGroup>
                            </Form>
                        </Container>
                        :
                        <Button variant={`outline-${theme.text}`} onClick={() => { setChangeUsername(true) }}>Change Username</Button>
                    }
                    <br /><br />
                    {isEmailAccount && 
                    <>
                        {changePassword ?
                            <Container>
                                <Form onSubmit={onPasswordSubmit}>
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                            <Button variant={`outline-${theme.text}`} type='submit'>Change Password</Button>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        type="password"
                                        id="Password"
                                        placeholder='New password'
                                        aria-label="New password"
                                        aria-describedby="basic-addon1"
                                    />
                                    <FormControl
                                        type="password"
                                        id="ConfirmPassword"
                                        placeholder='Confirm password'
                                        aria-label="Confirm password"
                                        aria-describedby="basic-addon1"
                                    />
                                </InputGroup>
                                </Form>
                            </Container>
                            :
                            <Button variant={`outline-${theme.text}`} onClick={() => { setChangePassword(true) }}>Change Password</Button>
                        }
                    </>}
                </div>
            </Jumbotron>

            {/* <CardGroup>
                {[1, 2, 3, 4].map(_ => {
                    return (
                        <Card key={_} style={{ width: '18rem' }} className={`m-1 bg-${theme.background}`}>
                            <Card.Body>
                                <Card.Title className="standartText">New Board</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">Create a new Kanban Board</Card.Subtitle>
                                <Card.Text className="standartText">
                                    Here we can have a description for each of the boards
                                    </Card.Text>
                                <hr />
                                <Link to="/profile/createBoard">New Board</Link>
                                <hr className={`bg-${theme.text}`} />
                                <Card.Link href="#">New Board</Card.Link>
                            </Card.Body>
                        </Card>
                    )
                })}
            </CardGroup>
            <hr className={`bg-${theme.text}`} /> */}
            <br />
            <Jumbotron style={{marginTop: '-20px'}} className={`bg-${theme.background}`}>
                <h2 className={`text-center`}>Search</h2>
                <Form onSubmit={onFormSubmit}>
                    <Form.Group>
                        <Form.Label className="my-1 mr-2">
                            Board Type
                        </Form.Label>
                        <Form.Control
                            as="select"
                            className="my-1 mr-sm-2"
                            custom
                            id='BoardType'
                        >
                            <option value="All">All</option>
                            <option value="kanban">KanBan</option>
                            <option value="mind-map">MindMap</option>
                            <option value="database">DataBase</option>
                            <option value="draw">Whiteboard</option>
                        </Form.Control>

                    </Form.Group>

                    <Form.Group>
                        <Form.Label sm={2}>
                            Search
                        </Form.Label>

                        <Form.Control id='BoardSearchString' type="text" placeholder="Search..." />
                    </Form.Group>

                    <Form.Group>
                        <Form.Check inline id='OnlyPrivate' label="Only Private Boards" type='checkbox' />
                        <Form.Check inline id='OnlyPublic' label="Only Public Boards" type='checkbox' />
                        <Form.Check inline id='OnlyCreated' label='Show Created' type='checkbox' />
                    </Form.Group>

                    <Button type="submit" variant={`outline-${theme.text}`} className="my-3 mx-2">Search</Button>
                    <Button  variant={`outline-${theme.text}`} onClick={() => {props.history.push('/profile/createBoard')}} className="my-3 mx-2">Create Board</Button>

            {/* <Link to='/profile/createBoard'> to create</Link> */}

                </Form>
            </Jumbotron>

            {createdOrGuest ?
                <>
                    <h4>Created </h4>
                    <hr className={`bg-${theme.text}`} />
                    {CreatedBoards.length ?
                        <CardColumns >
                            {CreatedBoards.map((board: any) => <BoardCard key={board.id} board={board}/>)}
                        </CardColumns>
                        :
                        <h6 className={`mb-4`}>You have no created boards</h6>}

                </>
                :
                <>
                    <h4>Guest </h4>
                    <hr className={`bg-${theme.text}`} />
                    {VistingBoards.length ?
                        <CardColumns >
                            {VistingBoards.map((board: any) => <BoardCard key={board.id} board={board} />)}
                        </CardColumns>
                        : <h6 className={`mb-4`}> You are not visiting any boards</h6>}
                </>
            }
        </Container>
    )
}

export default UserProfile;