import React from 'react';
import '../../static/css/NotFound.css';
import not_found from '../../static/img/NotFound.png';

const NotFound = () => {
  return (
    <div>
      <div className="not-found-wrapper">   
        <div className="not-found-container">
          <div className="login-reg-circles">
            <span className="r" />
            <span className="r-s" />
            <span className="l-s" />
            <span className="l" />
          </div>
          <div className="logRegPanel">
            <div className="auth-inner">
              <div className="no-found-ico"><img alt="Not Found" src={not_found} /></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}

export default NotFound;