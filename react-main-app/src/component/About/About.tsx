import React, {useContext} from 'react';

import { Card,  Button, Container, Jumbotron,  } from 'react-bootstrap';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import '../../static/css/About.css';

const About = (props: any) => {
  const {theme} = useContext(ThemeContext); 
  
  const openLink = (link: any) => {
    switch (link) {
      case 'firebase':
        window.open("https://firebase.google.com/?gclid=CjwKCAjwyo36BRAXEiwA24CwGWES3KcLvXS3BX4ggCWGk9SU-abJ2JA_EizIXaAOkn-6_QDcncSPdBoCMv8QAvD_BwE", "_blank");
        break;
      case 'jwt':
        window.open("https://jwt.io/", "_blank");
        break
      case 'reactjs':
        window.open("https://reactjs.org/", "_blank");
        break;
      case 'jsx':
        window.open("https://reactjs.org/", "_blank");
        break;
      case 'typescript':
        window.open("https://www.typescriptlang.org/", "_blank");
        break;
      case 'glue42':
        window.open("https://glue42.com/", "_blank");  
        break;
      case 'eslint':
        window.open("https://eslint.org/", "_blank");  
        break;
      case 'kanban':
        window.open("https://github.com/rcdexta/react-trello", "_blank");  
        break;
      case 'database-schema':
        window.open("https://github.com/cybertec-postgresql/react-database-diagram", "_blank");  
        break;
      case 'database-table':
        window.open("https://github.com/nadbm/react-datasheet", "_blank");  
        break;
      case 'drawing':
        window.open("https://www.typescriptlang.org/", "_blank");
        break;
      case 'mindmap':
        window.open("https://github.com/awehook/blink-mind-desktop", "_blank");
        break;
        case 'draw':
          window.open("https://github.com/tbolis/react-sketch", "_blank");
          break;
      default:
        break;
    }
  }

  return (
    <>
      <Jumbotron style={{ marginTop: '30px' }} className={`mx-3 bg-${theme.background}`}>
        <h1>These came in handy we hope they help you too! </h1>
        <hr />
        <h3 className='ml-4'>Technologies that we have used</h3>
      </Jumbotron>

      <Container style={{ paddingBottom: '30px' }}>
          <span className="rr" />
          <span className="rr-s" />
          <span className="ll-s" />
          <span className="ll" />
        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <span className="text-about">We have used</span> <Button className="btn-about" variant="danger" onClick={() => openLink('firebase')}>FireBase</Button> <span className="text-about">for all of our Back-End needs!</span> 🔥
            </Card.Text>
          </Card.Body>
        </Card>

        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <span className="text-about">Implements stateless authentication with</span> <Button className="btn-about" variant="info" onClick={() => openLink('jwt')}>JWT</Button> <span className="text-about">tokens.</span>
            </Card.Text>
          </Card.Body>
        </Card>

        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <Button className="btn-about" variant="success" onClick={() => openLink('typescript')}>TypeScript</Button> <span className="text-about">is used for more reliable code.</span>
            </Card.Text>
          </Card.Body>
        </Card>

          <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <span className="text-about">As a real-time user interface (UI) integration platform is used</span> <Button className="btn-about" variant="warning" onClick={() => openLink('glue42')}>Glue42</Button>
            </Card.Text>
          </Card.Body>
        </Card>

        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <Button className="btn-about" variant="primary" onClick={() => openLink('eslint')}>ESlint</Button> <span className="text-about">for code styling and coding rules.</span>
            </Card.Text>
          </Card.Body>
        </Card>

        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <span className="text-about">Kanban boards are based on</span> <Button className="btn-about" variant="success" onClick={() => openLink('kanban')}>react-trello</Button> <span className="text-about">component.</span>
            </Card.Text>
          </Card.Body>
        </Card>

        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <span className="text-about">Database designer utilize the</span> 
              <Button className="btn-about" variant="danger" onClick={() => openLink('database-schema')}>
                react-database-diagram
              </Button> <span className="text-about">and for editable table is used</span> 
              <Button className="btn-about" variant="info" onClick={() => openLink('database-table')}>react-datasheet</Button> <span className="text-about">component.</span>
          </Card.Text>
          </Card.Body>
        </Card>

        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <span className="text-about">For our drawing app we have used</span> <Button className="btn-about" variant="success" onClick={() => openLink('draw')}>react-sketch</Button> <span className="text-about"> library.</span>
            </Card.Text>
          </Card.Body>
        </Card>
 
        <Card className={`mx-1 card-about bg-${theme.background}`}>
          <Card.Body>
            <Card.Text>
              <span className="text-about"> For mind map board we used</span> <Button className="btn-about" variant="primary" onClick={() => openLink('mindmap')}>blink-mind</Button> <span className="text-about"></span>
            </Card.Text>
          </Card.Body>
        </Card> 

      </Container>
  </>
  );
}

export default About;