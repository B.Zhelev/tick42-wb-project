import React, { useState, useContext } from 'react'
import FireBaseApp from '../../constant/base';
import * as firebase from 'firebase/app'
import { withRouter } from 'react-router'
import '../../static/css/LoginReg.css';
import { store } from 'react-notifications-component';

import fb from '../../static/img/fb.png';
import google from '../../static/img/google.png';
import github from '../../static/img/github.png';
import github_dark from '../../static/img/github_dark.png';
import reg_ico from '../../static/img/sign_up.png';
import check from '../../static/img/checked.png';
import uncheck from '../../static/img/unchecked.png';
import { Button, Row } from 'react-bootstrap';
import { Input } from 'reactstrap';
import { ThemeContext } from '../../Context/Theme/ThemeContext';

//to do: add types
const Register = (props: any) => {

  const [imgChecked, setImgChecked] = useState(uncheck);
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [emailValid, setEmailValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const [confirmPasswordValid, setConfirmpasswordValid] = useState(false);
  const { theme } = useContext(ThemeContext)

  const providerGmail: any = new firebase.auth.GoogleAuthProvider();
  const providerFB = new firebase.auth.FacebookAuthProvider();
  const providerGitHub = new firebase.auth.GithubAuthProvider();

  const handleEmailChange = (e: any): void => {
    if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(e.target.value)) {
      setEmailValid(true);
    }
    else {
      setEmailValid(false);
    }
  }

  const handlePasswordChange = (e: any): void => {
    setPassword(e.target.value);
    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/.test(e.target.value)) {
      setPasswordValid(true);
    }
    else {
      setPasswordValid(false);
    }
    if (e.target.value === confirmPassword) {
      setConfirmpasswordValid(true);
    }
    else {
      setConfirmpasswordValid(false);
    }
  }

  const handleConfirmPasswordChange = (e: any): void => {
    setConfirmPassword(e.target.value);
    if (passwordValid && e.target.value === password) {
      setConfirmpasswordValid(true);
    }
    else {
      setConfirmpasswordValid(false);
    }
  }

  const toLogin = () => {
    props.history.push('/login')
  }

  const handleGoogleLogin = (): void => { //TODO: delete after inf dont need to use it but we'll need them soon
    firebase.auth().signInWithPopup(providerGmail)
    .then(function (result) {
      // The signed-in user info.
      //const user: any = result.user;
      props.history.push('/home');
    })
    .catch(function (error) {
      store.addNotification({
        title: 'Error',
        message: 'Registration faild',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    });
  }

  const handleFbLogin = (): void => { //TODO: delete after inf dont need to use it but we'll need them soon
    firebase.auth().signInWithPopup(providerFB).then(function (result) {
      // The signed-in user info.
      //const user: any = result.user;
      props.history.push('/home');
    }).catch(function (error) {
      store.addNotification({
        title: 'Error',
        message: 'Registration faild',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    });
  }

  const handleGitHubLogin = (): void => { //TODO: delete after inf dont need to use it but we'll need them soon
    firebase.auth().signInWithPopup(providerGitHub).then(function (result) {
      // The signed-in user info.
      //const user: any = result.user;
      props.history.push('/home');
      // ...
    }).catch(function (error) {
      store.addNotification({
        title: 'Error',
        message: 'Registration faild',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    });
  }

  const changeImg = (): void => {
    imgChecked === uncheck ? setImgChecked(check) : setImgChecked(uncheck);
  }

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    if (!passwordValid) {
      return store.addNotification({
        title: 'Error',
        message: 'Must contain at least one number, one uppercase and lowercase letter, one spacial character and from 8 to 15 characters!',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }

    if (!confirmPasswordValid) {
      store.addNotification({
        title: 'Error',
        message: `Your passwords doesn't match!`,
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
      return;
    }

    if (imgChecked !== check) {
      return store.addNotification({
        title: 'Error',
        message: 'You didnt accept the terms and policyes first!',
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }

    const { email, password } = event.target.elements;
    try {
      await FireBaseApp
        .auth()
        .createUserWithEmailAndPassword(email.value, password.value);
      props.history.push('/home')
    } catch (err) {
      store.addNotification({
        title: 'Error',
        message: err.message,
        type: 'danger',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }
  }

  return (
    <>
      <div className="log-reg-wrapper">
        <div className="log-reg-container">
          <div className="login-reg-circles">
            <span className="r" />
            <span className="r-s" />
            <span className="l-s" />
            <span className="l" />
          </div>
          <div className="logRegPanel">
            <div className={`auth-inner authInner bg-${theme.background}`}>
              <form onSubmit={handleSubmit}>
                <div className="log-reg-ico"><img alt="Not Found" src={reg_ico} /></div>
                <div className="log-reg-title standartText">Sign up</div>

                <div className="form-group">
                  <div className="labels-login-reg standartText">Email address</div>
                  <Input type="email" id='email' className={`logReg-form-control-${theme.background}`} placeholder="Enter email"
                    valid={emailValid ? true : false} onChange={handleEmailChange} />
                </div>

                <div className="form-group">
                  <div className="labels-login-reg standartText">Password</div>
                  <Input type="password" id='password' className={`logReg-form-control-${theme.background}`} placeholder="Enter password"
                    valid={passwordValid ? true : false} onChange={handlePasswordChange} />
                </div>

                <div className="form-group">
                  <div className="labels-login-reg standartText">Confirm Password</div>
                  <Input type="password" className={`logReg-form-control-${theme.background}`} placeholder="Enter password"
                    valid={confirmPasswordValid ? true : false} onChange={handleConfirmPasswordChange} />
                </div>

                <div className="login-reg-check">
                  <span><img id="img_check" src={imgChecked} onClick={changeImg} /></span><span className="standartText" id="img_check" onClick={changeImg}>Accept terms and conditions</span>
                </div>

                <div className="wrapper-login-reg">
                  <Button type="submit" className="btn btn-login-reg">Sign up</Button>
                </div>

                <div className="reg-have-account">
                  <Row>
                    <div className="forgot-password">Already have an account?</div>
                    <div className="log-reg-redirect" onClick={toLogin}>Login</div>
                  </Row>
                </div>

                <span className="auth-sep standartText"> or Sign up with one click </span>
                <div className="fast-icons">
                  <Row>
                    <div className="fb-ico" onClick={handleFbLogin}><img alt="Not Found" src={fb} /></div>
                    <div className="google-ico" onClick={handleGoogleLogin}><img alt="Not Found" src={google} /></div>
                    <div className="github-ico" onClick={handleGitHubLogin}><img alt="Not Found" src={theme.text === 'dark'
                      ? github
                      : github_dark} /></div>
                  </Row>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default withRouter(Register);