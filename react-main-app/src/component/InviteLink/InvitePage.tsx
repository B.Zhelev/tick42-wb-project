import React, { useState, useEffect, useContext } from 'react'
import * as firebase from "firebase"
import { useParams } from 'react-router-dom'
import GetSharableLink from './GetSharableLink'
import { TextField, Grid, Button } from '@material-ui/core'
import InvitedList from './InvitedList'
import { ThemeContext } from '../../Context/Theme/ThemeContext';


function InvitePage() {
  const [email, setEmail] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const [inviteEmails, setInviteEmails] = useState([])
  const id: any = useParams()

  const { theme } = useContext(ThemeContext);

  const db = firebase.firestore();
  useEffect(() => {
    const dbBoard = db.collection("boards").doc(id.boardId as string);
    dbBoard.get().then(doc => setInviteEmails(doc.data()!.invitedUsersEmail))

  }, [email])

  const submit = async () => {
    if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
      const dbBoard = await db.collection("boards").doc(id.boardId as string);
      const board = await dbBoard.get()
      const updateBoard = await board.data()
      updateBoard!.invitedUsersEmail.push(email)
      await dbBoard.update({ ...updateBoard })
      setEmail('');
    } else {
      setErrorMessage('Please enter correct email adress')

    }
  }

  const sendMessageOnEnter = (e: any) => {
    if (e.key === 'Enter') {
      submit();
    }
  }

  const errorHandler = (errorMessage: string) => {
    if (errorMessage !== "") {
      setTimeout(() => setErrorMessage(""), 5000)
      return <h4 style={{ color: "red" }} >{errorMessage}</h4>

    } else { return null }
  }



  return (
    <div className="log-reg-wrapper">
      <div className="log-reg-container">
        <div className="login-reg-circles">
          <span className="r" />
          <span className="r-s" />
          <span className="l-s" />
          <span className="l" />
        </div>
        <div>
          <div className={`auth-inner authInner bg-${theme.background}`}>
            <GetSharableLink />
            <br />
            <h1 style={{ margin: "5px" }}>Invite Page</h1>
            {errorHandler(errorMessage)}
            <Grid item xs={6} sm={3}>
              <TextField id="outlined-basic-email" placeholder="Email" label="BoardSharableLink" type='Email' fullWidth value={email} onKeyPress={sendMessageOnEnter} onChange={(e) => { setEmail(e.target.value) }} />
            </Grid>
            <Button style={{ margin: "6px" }} onClick={submit} variant="outlined">invite</Button>
            <InvitedList inviteEmails={inviteEmails} />
          </div>
          <br />
        </div>
      </div>
    </div>
  )
}

export default InvitePage;



