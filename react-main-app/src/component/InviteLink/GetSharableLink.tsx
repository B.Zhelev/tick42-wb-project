import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { store } from 'react-notifications-component';

function GetSharableLink() {
  const [clipboard, setClipboard] = useState('')

  const currentUrl = window.location.href.replace('/profile/invitepage/', `/${localStorage.getItem("typeBoard")}/`);

  const copyToClipboard = (text: string) => {
    var textField = document.createElement('textarea')
    textField.innerText = text
    document.body.appendChild(textField)
    textField.select()
    document.execCommand('copy')
    setClipboard(text)
    textField.remove()
    localStorage.clear()

  };
  

  // window.location.href.replace('/profile/invitepage/', '/kanban/')
  function SimpleSnackbar() {
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
      //setOpen(true);
      copyToClipboard(currentUrl);
      store.addNotification({
        title: 'Error',
        message: 'Link is Copied',
        type: 'success',                          // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                  // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    };

    const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
      if (reason === 'clickaway') {
        return;
      }

      setOpen(false);
    };

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          message="Link is Copied"
          action={
            <React.Fragment>
              <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
        <Button onClick={handleClick}>Get sharable link</Button>
      </div>
    );
  }
  return (
    <div>
      <SimpleSnackbar />
    </div>
  )
}

export default GetSharableLink;

