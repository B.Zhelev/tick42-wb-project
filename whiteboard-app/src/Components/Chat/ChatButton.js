import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import ChatIcon from '@material-ui/icons/Chat';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Chat from './Chat/Chat';

const useStyles = makeStyles((theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    
    },
  }),
);

export default function ChatButton() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div>
      <ChatIcon onClick={handleToggle}/>
      <Backdrop className={classes.backdrop} open={open} >
        <Chat handleClose={handleClose}/>
      </Backdrop>
    </div>
  );
}
