import React from 'react';
import { CompactPicker  } from 'react-color';

const AppColorPicker = ({color, onChangeComplete}) => {

    const setColor = (e) => {
        onChangeComplete(e.hex)
    };

    return (
        <CompactPicker  className='mx-auto' 
                    color={color}
                    onChangeComplete={setColor}/>
    );
};

export default AppColorPicker;