import React from "react";
import Button from '@material-ui/core/Button';
import * as firebase from "firebase";
export function SaveButton(props: any) {
  const saveMindMap = (e: any) => {
    const Id = window.location.pathname.split("/").pop();
    const { diagram } = props;
    const diagramProps = diagram.getDiagramProps();
    const { controller } = diagramProps;
    const json = controller.run("serializeModel", diagramProps);
    const jsonStr = JSON.stringify(json);

    const db = firebase.firestore();
    const dbBoard = db.collection("boards").doc(Id);
    dbBoard.update({ model: jsonStr });
  };

  return (
    <div>
      <Button onClick={saveMindMap}>
        Save
      </Button>
    </div>
  );
}
